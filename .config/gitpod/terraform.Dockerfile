# Utilisez une image de base Ubuntu 20.04
FROM ubuntu:20.04

# Mettez à jour le système et installez les outils nécessaires
RUN apt-get update -y && \
    apt-get install -y software-properties-common && \
    apt-add-repository --yes --update ppa:ansible/ansible && \
    apt-get install -y ansible && \
    apt-get install -y curl

# Installez Terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update -y && \
    apt-get install -y terraform

# Définissez des variables d'environnement si nécessaire
ENV TF_LOG=INFO

# Supprimez la mise en cache des paquets téléchargés
RUN apt-get clean

# Indiquez le répertoire de travail
WORKDIR /workspace

# Commande par défaut pour Gitpod
CMD ["sleep", "infinity"]
